**nEMU** [**n**curses interface for Q**EMU**]
===========

## Features
 * Install VM
 * Delete VM
 * Clone VM ( Broken in 1.1.0, fixed in master )
 * Show VM status
 * Start/stop/shutdown/reset VM
 * Connect to VM via vnc
 * Snapshots
 * Show/Edit VM settings
 * USB support
 * Network via tap interfaces

## Videos
[![Alt Install OpenBSD VM example](http://img.youtube.com/vi/GdqSk1cto50/1.jpg)](http://www.youtube.com/watch?v=GdqSk1cto50)
[![Alt Redirecting serial line terminals to tty,socket](http://img.youtube.com/vi/j5jeFa9Pl9E/1.jpg)](http://www.youtube.com/watch?v=j5jeFa9Pl9E)
[![Alt Snapshots preview](http://img.youtube.com/vi/lYkiolMg42Y/1.jpg)](http://www.youtube.com/watch?v=lYkiolMg42Y)
[![Alt Overview](http://img.youtube.com/vi/jOtCY--LEN8/1.jpg)](http://www.youtube.com/watch?v=jOtCY--LEN8)

## Environment Requirements
 * Linux/FreeBSD host
 * QEMU

## Packages
 * Debian and Ubuntu [packages](https://software.opensuse.org/download.html?project=home%3ASmartFinn%3AnEMU&package=nemu)
 * Gentoo: emerge app-emulation/nemu
