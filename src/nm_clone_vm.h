#ifndef NM_CLONE_VM_H_
#define NM_CLONE_VM_H_

void nm_clone_vm(const nm_str_t *name);

#endif /* NM_CLONE_VM_H_ */
/* vim:set ts=4 sw=4 fdm=marker: */
