#ifndef NM_ADD_VM_H_
#define NM_ADD_VM_H_

void nm_add_vm(void);
void nm_import_vm(void);

#endif /* NM_ADD_VM_H_ */
/* vim:set ts=4 sw=4 fdm=marker: */
