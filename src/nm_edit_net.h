#ifndef NM_EDIT_NET_H_
#define NM_EDIT_NET_H_

#include <nm_vm_control.h>

void nm_edit_net(const nm_str_t *name, const nm_vmctl_data_t *vm);

#endif /* NM_EDIT_NET_H_ */
/* vim:set ts=4 sw=4 fdm=marker: */
