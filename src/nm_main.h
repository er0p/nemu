#ifndef NM_MAIN_H_
#define NM_MAIN_H_

enum {
    NM_CHOICE_VM_LIST = 1,
    NM_CHOICE_VM_INST,
    NM_CHOICE_VM_IMPORT,
    NM_CHOICE_QUIT
};

enum nm_key {
    NM_KEY_ENTER = 10,
    NM_KEY_A = 97,
    NM_KEY_B = 98,
    NM_KEY_C = 99,
    NM_KEY_D = 100,
    NM_KEY_E = 101,
    NM_KEY_F = 102,
    NM_KEY_I = 105,
    NM_KEY_K = 107,
    NM_KEY_L = 108,
    NM_KEY_M = 109,
    NM_KEY_O = 111,
    NM_KEY_P = 112,
    NM_KEY_R = 114,
    NM_KEY_S = 115,
    NM_KEY_T = 116,
    NM_KEY_U = 117,
    NM_KEY_V = 118,
    NM_KEY_X = 120,
    NM_KEY_Y = 121,
    NM_KEY_Z = 122
};

#endif /* NM_MAIN_H_ */
/* vim:set ts=4 sw=4 fdm=marker: */
